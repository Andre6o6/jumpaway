﻿using UnityEngine;
using System.Collections;

public class BlockGenerating : MonoBehaviour {

	private static BlockGenerating _instance;
	public static BlockGenerating instance
	{
		get
		{
			if (_instance == null) Debug.LogError("Generator not found");
			return _instance;
		}
	}

	public GameObject block;
	public GameObject bonus;
	public GameObject enemy;
	public int obj_amount;
	public int bonus_rate;	// 1 bonus per 'bonus_rate' blocks 
	public int enemy_rate;	// 1 enemy per 'enemy_rate' blocks 
	bool active;


	// Use this for initialization
	void Awake () {
		_instance = this;
		active = true;		
	}
	
	// Update is called once per frame
	void Update () {
		if (!active && transform.position.x - PlayerController.instance.transform.position.x < 10)		//Distance to player
		{
			active = true;
		}
		
		if (active)
		{
			CreateBevel();
			active = false;
			if (enemy_rate > 1) enemy_rate--;
		}
	}

	void CreateBevel ()
	{
		int xOff;
		float yOff;
		float b_yOff = 1;
		int bonus_count = 0;	//amount of bonuses in a row
		Vector3 pos = transform.position;
		for (int i = 0; i < obj_amount; ++i)
		{
			xOff = 1; //I'll change it (no)
			yOff = Random.Range(-50, 51)/100f;
			pos.x += xOff * 2; pos.y += yOff * 2;
			Instantiate(block, pos, transform.rotation);

			if (Random.Range(0, enemy_rate) == 0)
			{
				Instantiate(enemy, new Vector3(pos.x,  pos.y + 2, 0), transform.rotation);
			}

			if (Random.Range(0, bonus_rate) == 0 && bonus_count == 0)
			{
				bonus_count = Random.Range (1,4);
				b_yOff = pos.y + Random.Range(2, 7);
			}
			if (bonus_count > 0) 
			{
				Instantiate(bonus, new Vector3(pos.x,  b_yOff, 0), transform.rotation);
				bonus_count--;
			}
		}
		transform.position = pos;
	}
}

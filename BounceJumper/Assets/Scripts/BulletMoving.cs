﻿using UnityEngine;
using System.Collections;

public class BulletMoving : MonoBehaviour {
	public GameObject target;
	public float speed;
	Vector3 dir = new Vector3(1,-1,0);
	Quaternion rot = new Quaternion();
	// Use this for initialization
	void Awake () {
		Destroy(gameObject, 5f);
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null)
		{
			dir = new Vector3(target.transform.position.x - transform.position.x,
			                  target.transform.position.y - transform.position.y, 0f).normalized;
		
			Vector3 angle = new Vector3 (0,0, Mathf.Atan((target.transform.position.y - transform.position.y)/(target.transform.position.x - transform.position.x)) * Mathf.Rad2Deg);
			rot.eulerAngles = angle;
		}
		transform.rotation = rot;
		transform.position += dir * speed; 
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "enemy") col.gameObject.GetComponent<EnemyMovement>().lives -=2;
		if (col.tag != "Player" && col.tag != "target") Destroy(gameObject); 
	}
}

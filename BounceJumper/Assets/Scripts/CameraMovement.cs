﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {
	public PlayerController player;
	Vector3 pos;

	// Use this for initialization
	void Start () {
	
	}

	void LateUpdate () {
		pos = player.transform.position;
		pos.z = transform.position.z;
		transform.position = pos;
	}
}

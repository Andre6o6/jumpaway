﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEditor.AnimatedValues;

[CustomEditor(typeof(PlayerUpgrader))]
public class UpgradeEditor : Editor {
	public AnimBool m_showFields = new AnimBool(false);

	public override void OnInspectorGUI()
	{
		var player = target as PlayerUpgrader;
		var width = GUILayout.Width (200);

		//if (player.upgrades.Count < 1) player.upgrades.Add (new PlayerUpgrader.Upgrade("", null, 0));
		EditorGUI.BeginChangeCheck ();

		player.gold = EditorGUILayout.IntField ("Gold", player.gold);

		EditorGUILayout.PrefixLabel("Upgrades count ");
		var count = EditorGUILayout.FloatField (player.upgrades.Count, width);

		while (count > player.upgrades.Count) player.upgrades.Add (new PlayerUpgrader.Upgrade("", null, 0));
		while (count < player.upgrades.Count) player.upgrades.Remove (player.upgrades[player.upgrades.Count - 1]);

		EditorGUI.indentLevel++;
		EditorGUILayout.BeginVertical ();

		m_showFields.target = EditorGUILayout.ToggleLeft("Show information", m_showFields.target);
		for (int i = 0; i < player.upgrades.Count; i++)
		{

			if (EditorGUILayout.BeginFadeGroup(m_showFields.faded))
			{
				EditorGUILayout.LabelField((i+1).ToString() + " upgrade", width);
				EditorGUI.indentLevel++;
				string name = player.upgrades[i].name;
				GameObject vis = player.upgrades[i].visual;
				int deg = player.upgrades[i].deg;

				EditorGUILayout.PrefixLabel("Name");
				name = EditorGUILayout.TextField(name, width );

				EditorGUILayout.PrefixLabel("Visualisation");
				vis = (GameObject)EditorGUILayout.ObjectField((Object)vis, typeof(GameObject), true, width);

				EditorGUILayout.PrefixLabel("Deg" );
				deg = EditorGUILayout.IntField(deg, width);

				PlayerUpgrader.Upgrade temp = new PlayerUpgrader.Upgrade (name, vis, deg);
				player.upgrades[i] = temp;
				EditorGUI.indentLevel--;
			}
			EditorGUILayout.EndFadeGroup();
		}
		EditorGUILayout.EndVertical();
		EditorGUI.indentLevel--;

		EditorUtility.SetDirty (player);
		AssetDatabase.SaveAssets ();
	}

}

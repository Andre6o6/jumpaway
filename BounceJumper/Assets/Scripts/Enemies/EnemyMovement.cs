﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {
	private Animator anim;
	public EnemyJump legCol;
	public EnemySense sense;
	public GameObject deadBody;
	public float speed;
	public float jumpForce;
	public int lives;
	private bool active = false;
	bool counted = false;

	// Use this for initialization
	void Start () {
		anim = this.gameObject.GetComponentInChildren<Animator>();
		anim.SetBool("isDead", false);
		lives = Random.Range(1, 4);
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x - PlayerController.instance.transform.position.x > 25)
			active = false;
		else active = true;

		if (lives > 0 && active)
		{
			if(sense.seeObstacle && legCol.onGround) 
				GetComponent<Rigidbody2D>().velocity = new Vector2 (0, jumpForce);
			transform.position += new Vector3(-speed, 0 ,0);
		}else if (lives <= 0) 
		{
			Die ();
		}
	}

	void Die()
	{
		anim.SetBool("isDead", true);
		if (!counted)
		{
			PlayerController.instance.score+= 2;
			counted = true;
		}
		var corp = (GameObject) Instantiate(deadBody, transform.position, transform.rotation);
		Destroy(gameObject);
		Destroy(corp.gameObject, 1f);
		//Instantiate smth
	}
}

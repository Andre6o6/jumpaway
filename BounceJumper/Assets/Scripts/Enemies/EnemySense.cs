﻿using UnityEngine;
using System.Collections;

public class EnemySense : MonoBehaviour {

	public bool seeObstacle;

	void OnTriggerStay2D(Collider2D col)
	{seeObstacle = true;}
	void OnTriggerExit2D(Collider2D col)
	{seeObstacle = false;}

}

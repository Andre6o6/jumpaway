﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {
	public Sprite sign;
	GUIStyle style;
	string score;
	public enum Stages {lobby, game, gameover}
	public Stages game_stage;

	// Use this for initialization
	void Start () {
		game_stage = Stages.lobby;
		PlayerController.instance.enabled = false;
		BlockGenerating.instance.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{	
		if (PlayerController.instance != null)
		{
			style = GUI.skin.textArea;
			style.fontSize = 20;
			score =  "Score: " + PlayerController.instance.score.ToString();
			GUI.Box(new Rect(0, 0, 100, 25), score , style);

			if (game_stage == Stages.lobby)
			{
				style.fontSize = 16;
				GUI.Box( new Rect (0, 34 * Screen.height/40f, Screen.width , 9 * Screen.height/40f ),
				        "\"It's time to leave this freaking planet! This machine will bounce me to freedom. But once turned on, it will never stop!\"\nPress DOWN_ARROW to grip the ground and squeeze the spring. More you hold, higher you'll jump.\nHold SPACE to aim and release SPACE to shoot. Good aiming takes time.", style );

				style.fontSize = 64;
				GUI.Box(new Rect(Screen.width/4f, Screen.height/4f, Screen.width/2f, Screen.height/2f), sign.texture);
				if (Input.GetKey(KeyCode.Space) || GUI.Button(new Rect (Screen.width / 4.0f, 3 * Screen.height/4f, Screen.width / 6.0f, Screen.height / 10f), "(SPACE to )Start"))
				{
					game_stage = Stages.game;
					PlayerController.instance.enabled = true;
					BlockGenerating.instance.enabled = true;
				}
				if (GUI.Button(new Rect (Screen.width / 4.0f + Screen.width / 6.0f , 3 * Screen.height/4f, Screen.width / 6.0f, Screen.height / 10f), "Control"))
				{
//					GUI.Box( new Rect (Screen.width / 8.0f, 31 * Screen.height/40f, 7 * Screen.width / 8.0f, 9 * Screen.height/40f ),
//					          "Press DOWNARROW to tighten the spring.\nMore you hold, higher you jump.\nHold SPACE to aim and release SPACE to shoot.", style );
//					GUI.Box( new Rect (0, 0,  Screen.width , Screen.height ),
//					        "Press DOWNARROW to tighten the spring.\nMore you hold, higher you jump.\nHold SPACE to aim and release SPACE to shoot.", style );
				}
				if (GUI.Button(new Rect (Screen.width / 4.0f + 2 * Screen.width / 6.0f , 3 * Screen.height/4f, Screen.width / 6.0f, Screen.height / 10f), "Exit"))
				{Application.Quit();}
			}

			if (!PlayerController.instance.isAlive)
			{
				style.fontSize = 32;
				if (game_stage == Stages.game) StartCoroutine(GameOver()); 

				if (game_stage == Stages.gameover) 
				{
					string epitaph = "You failed to escape the planet!\nYour score is " + PlayerController.instance.score.ToString() + ".\nPress SPACE to restart";
					GUI.Box(new Rect(Screen.width/4f, Screen.height/4f, Screen.width/2f, Screen.height/2f), epitaph , style);
					if (Input.GetKey(KeyCode.Space)) Application.LoadLevel (Application.loadedLevel);	
				}
			}
		}
	}
	IEnumerator GameOver()
	{
		yield return new WaitForSeconds(2f);
		game_stage = Stages.gameover;
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCabine : MonoBehaviour {
	private PlayerController player;
	public List<ParticleSystem> effects;
	// Use this for initialization
	void Start () {
		player = GetComponentInParent<PlayerController>();
		for (int i = 0; i < effects.Count; ++i)
			effects[i].Stop();
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < effects.Count; ++i)
		{
			effects[i].transform.position = transform.position;
		}
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		for (int i = 0; i < effects.Count; ++i)
		{
			if (!effects[i].isPlaying && player.isAlive) 
			{
				effects[i].Play();
			}
		}
		if (!player.sounds[1].isPlaying && player.isAlive) player.sounds[1].Play ();
		if (!col.collider.isTrigger) player.isAlive = false;
		GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0.5f);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

	private static PlayerController _instance;
	public static PlayerController instance
	{
		get
		{
			if (_instance == null) Debug.LogError("Player not found");
			return _instance;
		}
	}

	public Rigidbody2D playerRigid;
	public GameObject target;
	public GameObject bullet;
	public List<AudioSource> sounds;
	public bool onGround;
	public bool isAlive;
	public Vector2 dir;
	public float max_impulse;
	public float max_delay;
	public int score = 0;
	public float search_range;
	public float searching_time;
	public float aiming_time;

	public bool maneuvering_engines;

	private Animator anim;
	GameObject aim = null;
	Vector2 cur_dir; 
	private float impulse = 0;
	private float delay = 0;
	private float cur_time = 0;


	void Awake () {
		_instance = this;
		anim = this.gameObject.GetComponent<Animator>();
		impulse = 0;
		cur_dir = dir;
		isAlive = true;

	}
	
	void Update () {

		if (isAlive)
		{
//OLD SHOOTING			
//			if (Input.GetKeyDown (KeyCode.Space))
//			{
//				var enemies = GameObject.FindGameObjectsWithTag("enemy");
//				if (enemies != null)
//				{
//					GameObject closest_enemy = null;
//					var distance = Mathf.Infinity;
//					foreach (GameObject cur_enemy in enemies)
//					{
//						var cur_dist = (cur_enemy.transform.position - transform.position).magnitude;
//						if (cur_dist < distance)
//						{
//							distance = cur_dist; 
//							closest_enemy = cur_enemy;
//						}
//					}
//					if (distance < 15 && closest_enemy != null)
//					{
//						aim = (GameObject)Instantiate (target, closest_enemy.transform.position , transform.rotation);
//						aim.GetComponent<Targeting>().targeted_enemy = closest_enemy;
//					}else aim = null;
//				}else aim = null;
//			}
//			if (Input.GetKeyUp (KeyCode.Space))
//			{
//				if (aim != null)
//				{
//					var bull =(GameObject)Instantiate(bullet, transform.position, transform.localRotation);
//					bull.GetComponent<BulletMoving>().target = aim;
//					sounds[0].Play();	//laser sound
//					aim = null;
//				}
//			}

			if (onGround)
			{
				anim.SetBool("InAir", false);
//OLD AUTOMATIC JUMPING
//				anim.SetBool("StringDown", true);
//				if (Input.GetKey(KeyCode.DownArrow))
//				{
//					playerRigid.velocity = new Vector2(0, playerRigid.velocity.y);
//					delay++;
//					cur_dir.x -=0.01f;
//					impulse += 10f;
//					if (delay > max_delay * 5) delay = -2;
//				}else if (Input.GetKeyUp(KeyCode.DownArrow))
//				{
//					delay = -2;
//				}else
//					delay--;
//				if (delay < 0)
//				{
//					sounds[3].Play ();
//					anim.SetBool("StringDown", false);
//					onGround = false;
//					delay = max_delay;
//					playerRigid.AddForce(cur_dir * impulse);
//					impulse = max_impulse;
//					cur_dir = dir;
//				}

				if (Input.GetKeyDown(KeyCode.DownArrow))
				{
					anim.SetBool("StringDown", true);
					playerRigid.velocity = new Vector2(0, playerRigid.velocity.y);
				}
				if (Input.GetKey(KeyCode.DownArrow))
				{
					playerRigid.velocity = new Vector2(0, playerRigid.velocity.y);
					anim.SetBool("StringDown", true);
					if (impulse < max_impulse) 
						impulse += max_impulse * Time.deltaTime * 2;
					else
					{
						impulse += 10f;
						if (cur_dir.x > 0.01f) cur_dir.x -=0.01f;
						delay += Time.deltaTime;
						if (delay > max_delay)
						{
							sounds[3].Play ();
							anim.SetBool("StringDown", false);
							onGround = false;
							playerRigid.AddForce(cur_dir * impulse);
							impulse = 50;
							delay = 0;
							cur_dir = dir;
						}
					}

				}
				if (Input.GetKeyUp(KeyCode.DownArrow))
				{
					sounds[3].Play ();
					anim.SetBool("StringDown", false);
					onGround = false;
					playerRigid.AddForce(cur_dir * impulse);
					impulse = 50;
					delay = 0;
					cur_dir = dir;
				}

			}
			else
			{
				anim.SetBool("InAir", true);

//CONTROLLING YOUR ROTATION
				if (maneuvering_engines)
				{
					if (Input.GetKey (KeyCode.LeftArrow))
					{
						//transform.Rotate(0,0,1f);
						playerRigid.angularVelocity += 1;
					}
					if (Input.GetKey (KeyCode.RightArrow))
					{
						playerRigid.angularVelocity -= 1;
					}
				}

			}


		}
		if (!isAlive) Die ();
	}

	void LateUpdate () {
		if (isAlive)
		{
//SHOOTING
			cur_time -= Time.deltaTime;

			if (cur_time <= 0 && aim == null)
			{
				var enemies = GameObject.FindGameObjectsWithTag("enemy");
				if (enemies != null)
				{
					GameObject closest_enemy = null;
					var distance = Mathf.Infinity;
					foreach (GameObject cur_enemy in enemies)
					{
						var cur_dist = (cur_enemy.transform.position - transform.position).magnitude;
						if (cur_dist < distance)
						{
							distance = cur_dist; 
							closest_enemy = cur_enemy;
						}
					}
					if (distance < search_range && closest_enemy != null)
					{
						aim = (GameObject)Instantiate (target, closest_enemy.transform.position , transform.rotation);
						aim.GetComponent<Targeting>().targeted_enemy = closest_enemy;
					}else aim = null;
				}else aim = null;

				if (aim == null) cur_time = searching_time;
			}

			if (cur_time <= -aiming_time && aim != null)
			{
				var bull =(GameObject)Instantiate(bullet, transform.position, transform.localRotation);
				bull.GetComponent<BulletMoving>().target = aim;
				sounds[0].Play();	//laser sound
				aim = null;
				cur_time = 0;
			}
		}
	}

	void Die()
	{

	}
}

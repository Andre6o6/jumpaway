﻿using UnityEngine;
using System.Collections;

public class PlayerJumping : MonoBehaviour {
	public PlayerController player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "block")
			player.onGround = true;
		if (col.tag == "enemy" && PlayerController.instance.isAlive)
		{
			col.gameObject.GetComponent<EnemyMovement>().lives = 0;
			col.isTrigger = true;
			col.attachedRigidbody.velocity = Vector2.zero;
			col.attachedRigidbody.gravityScale = 0;
		}
		//player.playerRigid.velocity = Vector2.zero;
	}
}

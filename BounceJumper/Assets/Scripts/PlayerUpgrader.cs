﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerUpgrader : MonoBehaviour {

	private static PlayerUpgrader _instance;
	public static PlayerUpgrader instance
	{
		get{
			return _instance;
		}
	}

	public struct Upgrade
	{
		public Upgrade(string _name, GameObject vis, int n) {
			name = _name;
			visual = vis;
			deg = n;
		}
		public string name; 
		public GameObject visual; 	//the visual part of the up. Idk whether i should do it like GO or just sprite
		public int deg; 			//0 - not installed; 1 - bought; 2,3,... - more upgraded
	};

	public List<Upgrade> upgrades = new List<Upgrade>();
	public int gold;

	void Start () {
		_instance = this;

		for (int i = 0; i < upgrades.Count; ++i)
		{
			if (upgrades[i].deg != 0 && upgrades[i].visual != null)
			{
				var newUp = (GameObject)Instantiate(upgrades[i].visual, transform.position, transform.rotation);
				newUp.transform.parent = this.gameObject.transform;
			}
		}
	}
	
	void Update () {
		for (int i = 0; i < upgrades.Count; ++i)
		{
			if (upgrades[i].deg != 0 && upgrades[i].visual != null)
			{
				var newUp = (GameObject)Instantiate(upgrades[i].visual, transform.position, transform.rotation);
				newUp.transform.parent = this.gameObject.transform;
			}
		}
	}
}

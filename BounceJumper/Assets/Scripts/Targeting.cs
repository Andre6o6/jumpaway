﻿using UnityEngine;
using System.Collections;

public class Targeting : MonoBehaviour {
	public GameObject targeted_enemy;
	public float targeting_time;
//	public float max_targeting_time = 1f;
	Vector3 newScale;

	// Use this for initialization
	void Awake () {
//		targeting_time = max_targeting_time;
		newScale = 	transform.localScale;
		Destroy (gameObject, 5f);
	}

	// Update is called once per frame
	void LateUpdate () {
		if (PlayerController.instance.isAlive)
		{
			if (targeted_enemy != null)
				transform.position = targeted_enemy.transform.position;
//			targeting_time -= Time.deltaTime;

			targeting_time = PlayerController.instance.aiming_time;

			if (newScale.x > 0.5)
			{
				newScale.x -= (1 / targeting_time) * 0.5f * Time.deltaTime; 
				newScale.y -= (1 / targeting_time) * 0.5f * Time.deltaTime;
			}
			transform.localScale = newScale;
			//if (targeting_time <= 0) GetComponent<SpriteRenderer>().color = new Color();
		}else{
			if (targeted_enemy != null) transform.position = targeted_enemy.transform.position;
		}

	}
	void OnTriggerEnter2D(Collider2D col)
	{if (col.tag == "bullet" ) Destroy (gameObject);}
}

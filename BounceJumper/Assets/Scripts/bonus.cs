﻿using UnityEngine;
using System.Collections;

public class bonus : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			PlayerController.instance.score++;
			PlayerController.instance.sounds[2].Play ();
			Destroy(gameObject);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
